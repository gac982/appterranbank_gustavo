define({ 
  loadAccountList : function(){    
    var navManager = applicationManager.getNavigationManager();    
    var resultData = navManager.getCustomInfo("frmTransfer");    

    this.view.listFrom.masterData = resultData.masterData;    
    this.view.listTo.masterData = resultData.masterData;    
    this.view.forceLayout();  
  },
  
  updateAccountName : function() {
    var accountId = this.view.lblAccountId.text;
    var accountName = this.view.tbxName.text;
    var accountModule = applicationManager.getModule("AccountModule");
    AccountModule.presentationController.updateAccountName(accountId, accountName);
  }
  
});