define({ 

  //In frmHome controller
  //NOTE: Call this method during the frmHome preShow
  showAccountsData : function(){ 
    var navManager = applicationManager.getNavigationManager(); 
    var resultData = navManager.getCustomInfo("frmHome"); 
    alert("Data at frmHomeController ########"+JSON.stringify(resultData)); 
    var widgetDataMap = {   
      "lblAccountid": "accountidmasked",   
      "lblAccountname": "accountname",   
      "lblBalance": "balance" 
    };  
    this.view.segAccounts.widgetDataMap = widgetDataMap;  	 
    this.view.segAccounts.setData(resultData);  
  },
  //formHomeController and call from the segment click
  getTransactions : function(){    
    var transactionModule = applicationManager.getModule("TransactionModule");    	
    transactionModule.presentationController.getTransactionsAndShow();  
  },

  displayTransferForm: function(){
    var transferModule = applicationManager.getModule("TransferModule");
    transferModule.presentationController.getAccountsandLoadTransferForm();
  }

});