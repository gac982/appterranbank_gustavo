/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function(){
	var repoMapping = {
		account  : {
			model : "TerranBank_Service_V1/account/Model",
			config : "TerranBank_Service_V1/account/MF_Config",
			repository : "TerranBank_Service_V1/account/Repository",
		},
		transaction  : {
			model : "TerranBank_Service_V1/transaction/Model",
			config : "TerranBank_Service_V1/transaction/MF_Config",
			repository : "TerranBank_Service_V1/transaction/Repository",
		},
	};

	return repoMapping;
})