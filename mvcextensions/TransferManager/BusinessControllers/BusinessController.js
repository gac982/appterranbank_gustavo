//Business Controller TransferManager

define([], function () {
  
  function TransferManager() { 
    kony.mvc.Business.Controller.call(this); 
  } 
  inheritsFrom(TransferManager, kony.mvc.Business.Delegator); 
  TransferManager.prototype.initializeBusinessController = function() { 
  }; 
  TransferManager.prototype.execute = function(command) { 
    kony.mvc.Business.Controller.prototype.execute.call(this, command);
  };
  TransferManager.prototype.addTransactionToDB = function(params, presentationSuccessCallback,presentationErrorCallback){
    var self = this;
    alert("Input Params . "+JSON.stringify(params));
    var accountList =  kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("transaction");
    accountList.customVerb('doTransfer', params, addTransactionCompletionCallback);

    function  addTransactionCompletionCallback(status,  data,  error) {
      var srh = applicationManager.getServiceResponseHandler();
      var obj = srh.manageResponse(status, data, error);
      kony.print("Data from addTransaction ... "+JSON.stringify(obj));
      if (obj["status"] === true) {
        presentationSuccessCallback(obj["data"]);
      } else {
        presentationErrorCallback(obj["errmsg"]);
      }
    }
  };

  return TransferManager;
});