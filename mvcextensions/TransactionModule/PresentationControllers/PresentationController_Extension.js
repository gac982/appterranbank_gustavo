define({
  
  //New callback for skin
  //extension filename: TransactionModule_Extension
  getTransactionsSC : function(response){ 
    var data = response;  
    try {  
      var length = response.length;  
      for (var i = 0; i < length; i++) {  
        var amount = response[i].amount;  
        if ( (parseFloat(response[i].amount)) > 30){   
          response[i].amount = {   
            "text":amount,   
            "skin":"sknRedLabel"   
          };  
        }  
      } 
    } catch (ex) {  
      kony.print("Error: " + ex.toString()); } 
    var transactionNavObj = applicationManager.getNavigationManager(); 	 	 
    transactionNavObj.setCustomInfo("frmTransactions",response); 
    transactionNavObj.navigateTo("frmTransactions"); 
  }

});