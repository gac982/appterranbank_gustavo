// In accountManager BusinessController
define([], function () { 

  /**
     * User defined business controller
     * @constructor
     * @extends kony.mvc.Business.Controller
     */
  function AccountManager() { 

    kony.mvc.Business.Controller.call(this); 

  } 

  inheritsFrom(AccountManager, kony.mvc.Business.Delegator); 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller gets initialized
     * @method
     */
  AccountManager.prototype.initializeBusinessController = function() { 

  }; 

  /**
     * Overridden Method of kony.mvc.Business.Controller
     * This method gets called when business controller is told to execute a command
     * @method
     * @param {Object} kony.mvc.Business.Command Object
     */
  AccountManager.prototype.execute = function(command) { 

    kony.mvc.Business.Controller.prototype.execute.call(this, command);

  };

  AccountManager.prototype.getAccounts = function(presentationSuccessCB,presentationErrorCB) { 

    var self = this;
    var accountObj = kony.mvc.MDAApplication.getSharedInstance().getRepoManager().getRepository("account");
    accountObj.customVerb("getAccounts", {}, onCompletionCallBack);
    function onCompletionCallBack(status, data, errMsg){
      var resHandler = applicationManager.getServiceResponseHandler();
      var returnObj = resHandler.manageResponse(status,data,errMsg);
      kony.print("Data from getAccounts ... "+JSON.stringify(returnObj));      
      if(returnObj["status"] === true){
        presentationSuccessCB(returnObj["data"]);
      }else{
        presentationErrorCB(returnObj["errmsg"]);
      }
    }

  };    

  return AccountManager;
});