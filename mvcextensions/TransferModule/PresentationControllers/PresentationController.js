//Presentation Controller TransferModule

define([], function() {

  function Transfer_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Transfer_PresentationController, kony.mvc.Presentation.BasePresenter);

  Transfer_PresentationController.prototype.initializePresentationController = function() {

  };

  Transfer_PresentationController.prototype.getAccountsandLoadTransferForm = function() {    
   
    var navigationObj = applicationManager.getNavigationManager();    
    var response = navigationObj.getCustomInfo("frmHome");    
    var resList = [];       
    try{           
      var data = response;            
      var length = data.length; 
      var resObj = [];           
      for(var i=0;i<length;i++){                
        resObj=[                    
          data[i]["accountid"],          
          data[i]["accountname"] ];                  
        resList.push(resObj);            
      }           
    }catch(exp){          
      kony.print("error");        }    
    var listData = { "masterData":resList };   
    navigationObj.setCustomInfo("frmTransfer",listData);    
    navigationObj.navigateTo("frmTransfer");  
  };

  Transfer_PresentationController.prototype.saveTransactionToDB = function(fromaccountid,toaccountid,amount) {    
    var params = {            
      "tid": Math.random().toString(36).substring(7),            
      "fromaccountid": fromaccountid,            
      "toaccountid": toaccountid,            
      "amount":amount,            
      "notes" : "From MVC2.0 client"        
    };    
    var transferManger = applicationManager.getTransferManager();    	
    transferManger.addTransactionToDB(params,this.addTransactionSC.bind(this),this.addTransactionEC.bind(this));  
  };

  /* addTransactionSC */
  Transfer_PresentationController.prototype.addTransactionSC = function(response){    
    var navObj = applicationManager.getNavigationManager();    
    alert("Transaction Added Sucessfully #### . "+JSON.stringify(response));
    navObj.navigateTo("frmTransfer");  
  };  
  
  /* addTransactionEC */
  Transfer_PresentationController.prototype.addTransactionEC = function(errMsg){    
    var navObj = applicationManager.getNavigationManager();
    navObj.setCustomInfo("frmTransfer",errMsg);    
    alert("Error Added Transaction Record  ###### "+JSON.stringify(errMsg));
    navObj.navigateTo("frmTransfer");  
  };

  return Transfer_PresentationController;
});