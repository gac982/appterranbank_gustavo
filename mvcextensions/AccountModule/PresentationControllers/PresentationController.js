//AccountModule Presentation Controller
define([], function() {
  /**
     * User defined presentation controller
     * @constructor
     * @extends kony.mvc.Presentation.BasePresenter
     */
  function Account_PresentationController() {
    kony.mvc.Presentation.BasePresenter.call(this);
  }

  inheritsFrom(Account_PresentationController, kony.mvc.Presentation.BasePresenter);

  /**
     * Overridden Method of kony.mvc.Presentation.BasePresenter
     * This method gets called when presentation controller gets initialized
     * @method
     */
  Account_PresentationController.prototype.initializePresentationController = function() {

  };

  Account_PresentationController.prototype.getAccountsAndShowHome = function() {
    var accountManager = applicationManager.getAccountManager();
    accountManager.getAccounts(this.getAccountsSC.bind(this),this.getAccountsEC.bind(this));
  };
  
  Account_PresentationController.prototype.getAccountsSC = function(response) {
    var navigationManager = applicationManager.getNavigationManager();
    navigationManager.setCustomInfo("frmHome",response);
    navigationManager.navigateTo("frmHome");
  };

  Account_PresentationController.prototype.getAccountsEC = function(errMsg) {
    var navigationManager = applicationManager.getNavigationManager();
    navigationManager.setCustomInfo("frmHome",errMsg);
    navigationManager.navigateTo("frmHome");
  };
  
  return Account_PresentationController;
});