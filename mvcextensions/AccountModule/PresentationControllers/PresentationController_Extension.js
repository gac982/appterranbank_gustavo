define({
  //New Callback for Masking
  //extension filename: AccountModule_Extension
  //don't forget to edit the controller to use new field
  getAccountsSC : function(response){ 
    alert("In PC Extn"); 
    kony.print("#####Actual Acc Data " +JSON.stringify(response));  
    try{   
      var length=response.length;   
      for(var i=0;i<length;i++){    
        var accountidBeforeMask = response[i]["accountid"];    
        var l = accountidBeforeMask.length;    
        var firstPart = accountidBeforeMask.slice(0, l-2);    
        var secondPart = accountidBeforeMask.slice(l-2, l);    
        firstPart = firstPart.replace(/./g, "X"); 
        // replaces each char with X    
        response[i]["accountidmasked"] = firstPart + secondPart;
        kony.print("Modified Acc Data " +JSON.stringify(response));   
      }   
    } catch(exp){   
      kony.print("error "+JSON.stringify(exp));  }  
    var accNavObj = applicationManager.getNavigationManager(); 
    alert("Response at Account_PC . "+JSON.stringify(response)); 	
    accNavObj.setCustomInfo("frmHome",response); 
    accNavObj.navigateTo("frmHome"); 
  },
  
  navigateToUpdateAccForm : function() {  
    var navigationObj= applicationManager.getNavigationManager();  
    var response = navigationObj.getCustomInfo("frmHome");	
    var resList=[];  
    try{     
      var data = response;     
      var length=data.length; 		
      var resObj=[];     
      for(var i=0;i<length;i++) {      
        resObj=[data[i]["accountid"],     
                data[i]["accountname"] ];       
        resList.push(resObj);     }    
    }catch(exp){    
      kony.print("error");  }  
    var listData = { "masterData": resList };  
    navigationObj.setCustomInfo("frmChangeName",listData);  
    navigationObj.navigateTo("frmChangeName"); 
  },
  
  updateAccountName : function(accountId, accountName) {
    let params = {
      accountid : accountId,
      accountname : accountName
    };
    let accountManager = applicationManager.getAccountManager();
    accountManager.updateAccount(params, this.getUpdateAccountSC.bind(this).this.getUpdateAccountEC.bind(this));
  },
  
  /*  getAccounts Succes Callback  */
  getUpdateAccountSC : function(response) {
    alert("Account Name Update Successfully");
    let accountManager = applicationManager.getAccountManager();
    accountManager.getAccounts(this.getAccountsSC.bind(this).this.getAcountsEC.bind(this));
  },
  
  /*  getAccounts Error Callback  */
  getUpdateAccountEC : function(errMsg) {
    let accNavObj = applicationManager.getNavigationManager();
    //alert("Error at Account PC" + JSON.stringfy(response));
    accNavObj.setCustomInfo('frmHome', errMsg);
    alert("Unable to Update Account ######" +JSON.stringify(errMsg));
    accNavObj.navigateTo("frmHome");
  }
  
});